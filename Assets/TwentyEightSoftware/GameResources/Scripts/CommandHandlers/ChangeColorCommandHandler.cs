﻿using System;
using System.Collections;
using TwentyEightSoftware.GameResources.Scripts.Robot;
using UnityEngine;

namespace TwentyEightSoftware.GameResources.Scripts.CommandHandlers
{
    public class ChangeColorCommandHandler : ICommandHandler
    {
        public event Action OnCommandCompleted;

        private RobotController robotController;
        private Color targetColor;
        private float duration;

        private Coroutine changeColorCoroutine;

        public ChangeColorCommandHandler(RobotController robotController, Color targetColor, float duration)
        {
            this.robotController = robotController;
            this.targetColor = targetColor;
            this.duration = duration;
        }

        public void Execute()
        {
            changeColorCoroutine = robotController.StartCoroutine(ChangeColorCoroutine());
        }
        
        private IEnumerator ChangeColorCoroutine()
        {
            Color startColor = robotController.Renderer.material.color;
            float elapsedTime = 0f;

            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                float t = Mathf.Clamp01(elapsedTime / duration);
                robotController.Renderer.material.color = Color.Lerp(startColor, targetColor, t);
                yield return null;
            }

            robotController.Renderer.material.color = targetColor;
            robotController.StopCoroutine(changeColorCoroutine);
            OnCommandCompleted?.Invoke();
        }
    }
}