namespace TwentyEightSoftware.GameResources.Scripts.CommandHandlers
{
    public interface ICommandHandler
    {
        event System.Action OnCommandCompleted;
        void Execute();
    }
}
