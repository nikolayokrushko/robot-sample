﻿using System;
using System.Collections;
using TwentyEightSoftware.GameResources.Scripts.Robot;
using UnityEngine;

namespace TwentyEightSoftware.GameResources.Scripts.CommandHandlers
{
    public class MoveToPositionCommandHandler : ICommandHandler
    {
        public event Action OnCommandCompleted;
        
        private RobotController robotController;
        private Vector3 targetPosition;
        private float duration;

        private Coroutine moveCoroutine;

        public MoveToPositionCommandHandler(RobotController robotController, float xPosition, float zPosition,
            float duration)
        {
            this.robotController = robotController;
            targetPosition = new Vector3(xPosition, robotController.transform.position.y, zPosition);
            this.duration = duration;
        }

        public void Execute()
        {
            moveCoroutine = robotController.StartCoroutine(MoveCoroutine());
        }

        private IEnumerator MoveCoroutine()
        {
            Vector3 startPosition = robotController.transform.position;
            float elapsedTime = 0f;

            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                float t = Mathf.Clamp01(elapsedTime / duration);
                robotController.transform.position = Vector3.Lerp(startPosition, targetPosition, t);
                yield return null;
            }

            robotController.transform.position = targetPosition;
            robotController.StopCoroutine(moveCoroutine);
            OnCommandCompleted?.Invoke();
        }
    }
}