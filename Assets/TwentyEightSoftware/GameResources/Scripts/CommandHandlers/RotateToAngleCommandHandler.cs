using System;
using System.Collections;
using TwentyEightSoftware.GameResources.Scripts.Robot;
using UnityEngine;

namespace TwentyEightSoftware.GameResources.Scripts.CommandHandlers
{
    public class RotateToAngleCommandHandler : ICommandHandler
    {
        public event Action OnCommandCompleted;

        private RobotController robotController;
        private float angle;
        private float duration;

        private Coroutine rotateCoroutine;

        public RotateToAngleCommandHandler(RobotController robotController, float angle, float duration)
        {
            this.robotController = robotController;
            this.angle = angle;
            this.duration = duration;
        }

        public void Execute()
        {
            rotateCoroutine = robotController.StartCoroutine(RotateCoroutine());
        }
        
        private IEnumerator RotateCoroutine()
        {
            Quaternion startRotation = robotController.transform.rotation;
            Quaternion targetRotation = Quaternion.Euler(0f, angle, 0f);
            float elapsedTime = 0f;

            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                float t = Mathf.Clamp01(elapsedTime / duration);
                robotController.transform.rotation = Quaternion.Slerp(startRotation, targetRotation, t);
                yield return null;
            }

            robotController.transform.rotation = targetRotation;
            robotController.StopCoroutine(rotateCoroutine);
            OnCommandCompleted?.Invoke();
        }
    }
}