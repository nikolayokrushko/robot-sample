using System.Collections.Generic;
using TwentyEightSoftware.GameResources.Scripts.CommandHandlers;
using TwentyEightSoftware.GameResources.Scripts.SOCommands;
using UnityEngine;
using UnityEngine.Serialization;

namespace TwentyEightSoftware.GameResources.Scripts.Robot
{
    public class RobotController : MonoBehaviour
    {
        [SerializeField] private Renderer robotRenderer;
        [SerializeField] private List<CommandBase> commands;

        public Renderer Renderer => robotRenderer;

        private readonly Queue<ICommandHandler> commandHandlers = new Queue<ICommandHandler>();

        private void Start()
        {
            foreach (var сommand in commands)
            {
                switch (сommand.GetType())
                {
                    case CommandType.MoveToPosition:
                        var moveData = сommand as MoveToPositionCommand;
                        var moveHandler =
                            new MoveToPositionCommandHandler(this, moveData.XPosition, moveData.ZPosition,
                                moveData.Duration);
                        commandHandlers.Enqueue(moveHandler);
                        break;
                    case CommandType.RotateToAngle:
                        var rotateData = сommand as RotateToAngleCommand;
                        var rotateHandler =
                            new RotateToAngleCommandHandler(this, rotateData.Angle, rotateData.Duration);
                        commandHandlers.Enqueue(rotateHandler);
                        break;
                    case CommandType.ChangeColor:
                        var colorData = сommand as ChangeColorCommand;
                        var changeColorHandler =
                            new ChangeColorCommandHandler(this, colorData.Color, colorData.Duration);
                        commandHandlers.Enqueue(changeColorHandler);
                        break;
                }
            }

            ExecuteNextCommand();
        }

        private void ExecuteNextCommand()
        {
            if (commandHandlers.Count > 0)
            {
                var nextHandler = commandHandlers.Dequeue();
                nextHandler.OnCommandCompleted += OnCommandCompleted;
                nextHandler.Execute();
            }
        }

        private void OnCommandCompleted()
        {
            if (commandHandlers.Count > 0)
            {
                var completedHandler = commandHandlers.Peek();
                completedHandler.OnCommandCompleted -= OnCommandCompleted;
                ExecuteNextCommand();
            }
            else
            {
                Debug.Log("All commands completed.");
            }
        }
    }
}