using UnityEngine;

namespace TwentyEightSoftware.GameResources.Scripts.SOCommands
{
    [CreateAssetMenu(fileName = "Change Color Command", menuName = "Robot/Change Color Command")]
    public class ChangeColorCommand : CommandBase
    {
        public Color Color;
        
        public override CommandType GetType()
        {
            return CommandType.ChangeColor;
        }
    }
}