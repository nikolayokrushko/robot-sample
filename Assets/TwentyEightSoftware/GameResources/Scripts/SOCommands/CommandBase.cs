﻿using UnityEngine;

namespace TwentyEightSoftware.GameResources.Scripts.SOCommands
{
    public abstract class CommandBase : ScriptableObject
    {
        public float Duration;

        public abstract CommandType GetType();
    }
}