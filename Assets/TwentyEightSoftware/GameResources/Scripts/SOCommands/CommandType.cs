namespace TwentyEightSoftware.GameResources.Scripts.SOCommands
{
    public enum CommandType
    {
        MoveToPosition,
        RotateToAngle,
        ChangeColor
    }
}