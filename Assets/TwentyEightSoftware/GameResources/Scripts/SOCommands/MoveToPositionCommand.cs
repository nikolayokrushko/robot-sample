using UnityEngine;

namespace TwentyEightSoftware.GameResources.Scripts.SOCommands
{
    [CreateAssetMenu(fileName = "Move Command", menuName = "Robot/Move To Command")]
    public class MoveToPositionCommand : CommandBase
    {
        [Range(-45f, 45f)]
        public float XPosition;
        [Range(-45f, 45f)]
        public float ZPosition;

        public override CommandType GetType()
        {
            return CommandType.MoveToPosition;
        }
    }
}