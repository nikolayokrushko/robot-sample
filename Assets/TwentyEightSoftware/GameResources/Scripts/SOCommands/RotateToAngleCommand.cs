﻿using UnityEngine;

namespace TwentyEightSoftware.GameResources.Scripts.SOCommands
{
    [CreateAssetMenu(fileName = "Rotate To Command", menuName = "Robot/Rotate To Command")]
    public class RotateToAngleCommand : CommandBase
    {
        public float Angle;
        
        public override CommandType GetType()
        {
            return CommandType.RotateToAngle;
        }
    }
}