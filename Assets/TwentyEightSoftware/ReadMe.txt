Made on the principle of a cassette insert.
We can insert any instruction for our objects that will be played back. The system does not care how many instructions there are. All instructions work in turn.

Flow for adding commands:
1. Add our command type to the CommandType.cs enumerator
2. Create a new command script with data, which should be inherited from the CommandBase.cs class. We prescribe the fields that will be used for this instruction.
3. We create an command handler class that must implement the ICommandHandler interface and implement the behavior logic for the robot when applying this instruction to the robot.
4. In the RobotController class, in the Start method, we add our created instruction for its correct processing
5. In the Project window, right-click on an empty space and create a ScriptableObject Command. Setting up incoming data
6. Drag the received object to our entity robot in the list of instructions
7. Click play
6. Enjoy the result